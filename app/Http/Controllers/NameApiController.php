<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Name;

class NameApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $names = Name::all();
        $namesFormatted = $names->map(function ($name) {
        return [
          'id' => $name->id,
          'name' => $name->name,
          'total' => intval($name->total),
          'totalDisplay' => number_format($name->total),
          'maleShare' => $name->maleShare,
          'maleShareDisplay' => number_format(($name->maleShare * 100), 2).'%',
          'femaleShare' => $name->femaleShare,
          'femaleShareDisplay' => number_format(($name->femaleShare * 100), 2).'%',
          'gap' => $name->gap,
          'gapDisplay' => number_format(($name->gap * 100), 2),
          'maleCount' => intval($name->total * $name->maleShare),
          'femaleCount' => intval($name->total - $name->maleCount),
        ];
      });

        return $namesFormatted->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      *
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      *
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         $name = Name::find($id);
         $name->totalDisplay = number_format($name->total);
         $name->maleShareDisplay = number_format(($name->maleShare * 100), 2).'%';
         $name->femaleShareDisplay = number_format(($name->femaleShare * 100), 2).'%';
         $name->gapDisplay = number_format(($name->gap * 100), 2);
         $name->maleCount = intval($name->total * $name->maleShare);
         $name->femaleCount = intval($name->total - $name->maleCount);
         $name->maleCountDisplay = number_format($name->maleCount);
         $name->femaleCountDisplay = number_format($name->femaleCount);

         return $name;
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
}
