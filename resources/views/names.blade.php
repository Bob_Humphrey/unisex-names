@extends('app')

@section('content')

<div id="names-page">
  <h2>Names</h2>

  <div class="row">
    <div class="col-md-4">
      <div id="name-detail" v-bind:style="styleNameDetail">
        <div id="name-detail-name" class="hide">
          @{{name.name}}
        </div>
        <canvas id="name-detail-chart">
        </canvas>
        <div id="name-detail-breakdown" class="hide">
          <div id="name-detail-male">
            MALES (@{{name.maleShareDisplay}}) - @{{name.maleCountDisplay}}
          </div>
          <div id="name-detail-female">
            FEMALES (@{{name.femaleShareDisplay}}) - @{{name.femaleCountDisplay}}
          </div>
        </div>
      </div>
    </div>

  <div class="col-md-8">
  <form id="search">
    Search <input name="query" v-model="nameSearch">
  </form><br />

  <div id="name-table-container">
  <table id="name-table" class="table table-striped hide" >
    <thead>
      <tr>
        <th
          @click="sortByNames('id')"
          :class="{active: nameSortKey == 'id'}">
          Rank
          <span class="arrow"
            :class="nameSortRows['id'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>

        <th
          @click="sortByNames('name')"
          :class="{active: nameSortKey == 'name'}">
          Name
          <span class="arrow"
            :class="nameSortRows['name'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>

        <th
          @click="sortByNames('total')"
          :class="{active: nameSortKey == 'total'}">
          Total
          <span class="arrow"
            :class="nameSortRows['total'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>

        <th
          @click="sortByNames('maleShare')"
          :class="{active: nameSortKey == 'maleShare'}">
          Male Share
          <span class="arrow"
            :class="nameSortRows['maleShare'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>

        <th
          @click="sortByNames('femaleShare')"
          :class="{active: nameSortKey == 'femaleShare'}">
          Female Share
          <span class="arrow"
            :class="nameSortRows['femaleShare'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>

        <th
          @click="sortByNames('gap')"
          :class="{active: nameSortKey == 'gap'}">
          Gap
          <span class="arrow"
            :class="nameSortRows['gap'] > 0 ? 'asc' : 'dsc'">
          </span>
        </th>


      </tr>
    </thead>
    <tbody>
      <tr v-for="
        name in names
        | filterBy nameSearch
        | orderBy nameSortKey nameSortRows[nameSortKey]"
        v-on:click="nameSelected(name.id)"
        >
        <td>
          @{{name.id}}
        </td>
        <td>
          @{{name.name}}
        </td>
        <td>
          @{{name.totalDisplay}}
        </td>
        <td>
          @{{name.maleShareDisplay}}
        </td>
        <td>
          @{{name.femaleShareDisplay}}
        </td>
        <td>
          @{{name.gapDisplay}}
        </td>
      </tr>
    </tbody>
  </table>
</div>
</div>
</div>
</div>

@stop
