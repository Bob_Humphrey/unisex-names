@extends('app')

@section('content')

<div id="home-page">

  <h2>Unisex Names</h2>

  <h3>Description</h3>

  <p>This application is an exercise in using the Vue.js development framework.</p>

  <p>FiveThirtyEight.com crunched 100 years of data from the Social Security
    Administration to determine that about 3 million Americans have unisex names.
    A unisex name--acording to their definition--is one that is shared by both
    sexes, with one sex having at least 33% of all the people with that name.</p>

  <p>There is additional information on
    <a href="http://fivethirtyeight.com/features/there-are-922-unisex-names-in-america-is-yours-one-of-them/"
    target="_blank"> their website</a>.</p>

  <h3>Built With</h3>

  <p>PHP, Laravel, JavaScript, Vue.js, MySQL, and Bootstrap.  The data is
    provided by FiveThirtyEight.com and made available under the
    <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">
    Creative Commons Attribution 4.0 International License</a>.</p>

  <h3>To Use</h3>

  Select NAMES from the main menu.  The displayed chart can be sorted on any
  column.  Click on any name to see detailed information for that name.





</div>


@stop
