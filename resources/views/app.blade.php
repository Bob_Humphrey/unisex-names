<!DOCTYPE html>
<html>
<head class="backdrop">
  <title>Unisex Names</title>
  <link rel="stylesheet" href="/css/all.css">
</head>
<body>
    @include('partials.navbar')
    <div id="app" class="container">
    @yield('content')
    </div>
    @include('partials.footer')
</body>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
<script src="/js/all.js"></script>
<!--  main.js used in development only
<script src="/js/main.js"></script> -->
</html>
