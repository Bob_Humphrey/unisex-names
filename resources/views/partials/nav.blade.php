<?php
$homeLink = url('/home');
$namesLink = url('/names');
?>

<div id="menu">
  <a href="{{$homeLink}}" type="button" class="btn btn-link" >Home</a>
  <a href="{{$namesLink}}" type="button" class="btn btn-link" >Names</a>
</div>
