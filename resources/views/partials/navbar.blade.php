<?php

$homeLink = url('/home');
$namesLink = url('/names');

// Set the current page as active.
$activeNames = '';
$path = ($_SERVER['REQUEST_URI']);

$pathNames = '/names';

if (substr($path, 0, strlen($pathNames)) == $pathNames) {
    $activeNames = 'active';
}

?>

<nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://bobhumphrey.org"><img src="/images/logo-80.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="{{$homeLink}}">
            <span class="nav-item visible-lg-inline">UNISEX NAMES</span>
          </a>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li class="{{$activeNames}}"><a href="{{$namesLink}}">NAMES</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
