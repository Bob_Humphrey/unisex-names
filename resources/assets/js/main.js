var Vue = require('vue');
Vue.use(require('vue-resource'));

var admin = new Vue({
  el: '#app',

  data: {

    chartData:  [
      {
        value: 0.0,
        color: "#0D47A1",
      },
      {
        value: 0.0,
        color:"#039BE5",
      },
    ],

    name: {
      id: 0,
      name: '',
      total: '',
      maleShare: 0.0,
      femaleShare: 0.0,
      maleShareDisplay: '',
      femaleShareDisplay: '',
      gap: '',
      maleCountDisplay: '',
      femaleCountDisplay: '',
    },
    nameDBFail: false,
    nameGridColumns: ['id', 'name', 'total', 'maleShare', 'femaleShare', 'gap'],
    names: [],
    nameSearch: '',
    nameSortKey: '',
    nameSortRows: {
      id: -1,
      name: -1,
      total: -1,
      maleShare: -1,
      femaleShare: -1,
      gap: -1
    },
    nameSuccess: false,
    nameSuccessMessage: '',
    styleNameDetail: {
      border: 'none'
    }
  },

  computed: {

  },

  ready:  function () {
    this.fetchNames()
  },

  methods: {

    // NAMES

    fetchName: function(id) {
      this.$http.get('/api/names/' + id)
        .then((response) => {
          // API action success.
          console.log(response.data)
          this.name.id = id
          this.name.name = response.data.name
          this.name.total = response.data.totalDisplay
          this.name.maleShare = response.data.maleShare
          this.name.femaleShare = response.data.femaleShare
          this.name.maleShareDisplay = response.data.maleShareDisplay
          this.name.femaleShareDisplay = response.data.femaleShareDisplay
          this.name.gap = response.data.gapDisplay
          this.name.maleCountDisplay = response.data.maleCountDisplay
          this.name.femaleCountDisplay = response.data.femaleCountDisplay
          this.chartData[0].value = response.data.femaleShare
          this.chartData[1].value = response.data.maleShare
          var ctx = document.getElementById("name-detail-chart").getContext("2d");
          Chart.defaults.global.animation = false;
          Chart.defaults.global.showTooltips = false;
          Chart.defaults.global.responsive = true;
          new Chart(ctx).Pie(this.chartData);
          // Don't show the detail until the data is available.
          // I do it this way to prevent the template from being flashed
          // for an instance on the screen.
          // The chart container must always be visible or the chart will not
          // be drawn.
          let hasNameDetail = document.getElementById("name-detail");
          if (hasNameDetail) {
            console.log(this.name)
            document.getElementById("name-detail-name").className =
            document.getElementById("name-detail-name").className.replace(/\bhide\b/,'')
            document.getElementById("name-detail-breakdown").className =
            document.getElementById("name-detail-breakdown").className.replace(/\bhide\b/,'')
            this.styleNameDetail.border = "1px solid #bbb"
          }
        }, (response) => {
          // API action fail.
          self = this
          this.nameDBFail = true
          setTimeout(function () {
            self.nameDBFail = false
          }, 5000)
        })
    },

    fetchNames: function() {
      this.$http.get('/api/names')
      .then((response) => {
        // API action success.
        this.names = response.data
        // Don't show the grid until the data is available.
        // I do it this way to prevent the template from being flashed
        // for an instance on the screen.
        let hasNameTable = document.getElementById("name-table");
        if (hasNameTable) {
          document.getElementById("name-table").className =
          document.getElementById("name-table").className.replace(/\bhide\b/,'')
        }
      }, (response) => {
        // API action fail.
        self = this
        this.nameDBFail = true
        setTimeout(function () {
          self.nameDBFail = false
        }, 5000)
      })
    },

    nameSelected: function(id) {
      this.fetchName(id)
    },

    sortByNames: function (key) {
      this.nameSortKey = key
      let nameSortRows = this.nameSortRows
      nameSortRows[key] = nameSortRows[key] * -1
      this.nameSortRows = nameSortRows
    },

  }


});
