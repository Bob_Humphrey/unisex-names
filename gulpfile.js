var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
  mix.sass([
    'app.scss'
  ], 'resources/assets/css');

  mix.styles([
    'libs/bootstrap-3-3-5.min.css',
    'libs/font-awesome.css',
    'app.css'
  ]);
});

elixir(function(mix) {
    mix.scripts([
        'libs/jquery-2.1.4.min.js',
        'libs/bootstrap-3-3-5.min.js'
    ]);
});

elixir(function(mix) {
    mix.browserify(
      'main.js'
    );
});

// Use the scriptsIn function only for gulp, not gulp watch.
// Will cause a loop when using gulp watch.

elixir(function(mix) {
     mix.scriptsIn('public/js');
});
